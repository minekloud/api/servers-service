import {MigrationInterface, QueryRunner} from "typeorm";

export class ServerTableTest1608026093546 implements MigrationInterface {
    name = 'ServerTableTest1608026093546'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "server_status_enum" AS ENUM('ON', 'OFF', 'STARTING IN PROGRESS')`);
        await queryRunner.query(`CREATE TABLE "server" ("id" SERIAL NOT NULL, "idUser" integer NOT NULL, "name" character varying NOT NULL, "hostname" character varying NOT NULL, "status" "server_status_enum" NOT NULL DEFAULT 'OFF', "maxAvailableRAM" numeric NOT NULL, CONSTRAINT "UQ_23a8f3a81e2bda5622ee620f01e" UNIQUE ("idUser"), CONSTRAINT "UQ_290f74df405c1318c5fc0cc170e" UNIQUE ("hostname"), CONSTRAINT "PK_f8b8af38bdc23b447c0a57c7937" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "server"`);
        await queryRunner.query(`DROP TYPE "server_status_enum"`);
    }

}
