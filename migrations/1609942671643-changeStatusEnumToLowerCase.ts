import {MigrationInterface, QueryRunner} from "typeorm";

export class changeStatusEnumToLowerCase1609942671643 implements MigrationInterface {
    name = 'changeStatusEnumToLowerCase1609942671643'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TYPE server_status_enum RENAME VALUE 'OFF' TO 'off'
        `);

        await queryRunner.query(`
            ALTER TYPE "public"."server_status_enum"
            RENAME VALUE 'ON' TO 'on'
        `);

        await queryRunner.query(`
            ALTER TYPE "public"."server_status_enum"
            RENAME VALUE 'STARTING IN PROGRESS' TO 'loading'
        `);
        
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
        ALTER TYPE "public"."server_status_enum"
        RENAME VALUE 'off' TO 'OFF'
    `);

    await queryRunner.query(`
        ALTER TYPE "public"."server_status_enum"
        RENAME VALUE 'on' TO 'ON'
    `);

    await queryRunner.query(`
        ALTER TYPE "public"."server_status_enum"
        RENAME VALUE 'loading' TO 'STARTING IN PROGRESS'
    `);
    }

}
