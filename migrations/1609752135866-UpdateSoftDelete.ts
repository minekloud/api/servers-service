import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateSoftDelete1609752135866 implements MigrationInterface {
    name = 'UpdateSoftDelete1609752135866'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
                RENAME COLUMN "isDeleted" TO "deletedAt"
        `);
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "deletedAt"
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "deletedAt" TIMESTAMP
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "deletedAt"
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "deletedAt" boolean NOT NULL DEFAULT false
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
                RENAME COLUMN "deletedAt" TO "isDeleted"
        `);
    }

}
