import {MigrationInterface, QueryRunner} from "typeorm";

export class MaxRamDecimal1611075838657 implements MigrationInterface {
    name = 'MaxRamDecimal1611075838657'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "maxAvailableRAM" TYPE numeric(3, 1)
        `);
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."maxAvailableRAM" IS NULL
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            COMMENT ON COLUMN "server"."maxAvailableRAM" IS NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ALTER COLUMN "maxAvailableRAM" TYPE numeric(2)
        `);
    }

}
