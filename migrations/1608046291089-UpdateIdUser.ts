import {MigrationInterface, QueryRunner} from "typeorm";

export class UpdateIdUser1608046291089 implements MigrationInterface {
    name = 'UpdateIdUser1608046291089'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server" DROP CONSTRAINT "UQ_23a8f3a81e2bda5622ee620f01e"
        `);
        await queryRunner.query(`
            ALTER TABLE "server" DROP COLUMN "idUser"
        `);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD "idUser" integer NOT NULL
        `);
        await queryRunner.query(`
            ALTER TABLE "server"
            ADD CONSTRAINT "UQ_23a8f3a81e2bda5622ee620f01e" UNIQUE ("idUser")
        `);
    }

}
