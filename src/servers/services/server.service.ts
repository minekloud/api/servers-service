import { Injectable, HttpException, NotFoundException, ConflictException } from '@nestjs/common';
import {Server} from '../entities/server.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { DeepPartial, Repository } from 'typeorm';
import { HyperkubeService } from './hyperkube.service';
import { PlayerService } from './player.service'
import { ServerDto } from '../dto/server.dto'
@Injectable()
export class ServerService {

  constructor(
    @InjectRepository(Server)
    private serversRepository: Repository<Server>,
    private hyperkubeService: HyperkubeService,
    private playerService: PlayerService,
    ) {}

  //Get all existing servers in BDD
  //Return list of server objects;  return null if not found
  async find(query): Promise<ServerDto[]> {
    let result: ServerDto[] = []

    if (Object.keys(query).length > 0) {
      let deletedItems = query["withDelete"] != undefined ? true : false;
      if (deletedItems) {
        delete query.withDelete
      }
      result =  await this.serversRepository.find({where: query,withDeleted:deletedItems, order: {id: 'ASC'}}) as ServerDto[]      
    } else {
      result = await this.serversRepository.find() as ServerDto[] 
    }
    
    return result.map(server => {
      server.players = this.playerService.getPlayersByServer(server.id)
      return server
    })
  }

  //Get server with specific ID in BDD
  //Return serveur object if found, otherwise return null
  async findById(id: number) : Promise<ServerDto>{
    const server = await this.serversRepository.findOne(id) as ServerDto
    if(server) {
      server.players = this.playerService.getPlayersByServer(server.id)
    }  
    return server
  } 

  //Create new serveur
  //Return null exception if success; otherwise return HttpException
  async create(partialServer: DeepPartial<Server>) : Promise<HttpException> {
    //Check hostname is free
    if (await this.serversRepository.findOne({where: {hostname: partialServer.hostname}})) {
      return new ConflictException("Another server has already this hostname ("+partialServer.hostname+")");
    }
    
    let server = this.serversRepository.create(partialServer);

    //Add server in BDD
    server = await this.serversRepository.save(server);
    //Notify HyperKube, don't wait for awnser
    this.hyperkubeService.notifyHyperkube(server.id.toString());

    return null;
  }

  //Update some server informations
  //If success return server object updated; otherwise return HttpException;
  async update(id: number,partialServer: DeepPartial<Server>) : Promise<ServerDto | HttpException> {
    let server = await this.serversRepository.preload(({id,...partialServer}));
    if (!server) 
      return new NotFoundException("No server with ID: "+id+" found !");
    
    //Notify HyperKube, don't wait for awnser
    this.hyperkubeService.notifyHyperkube(id.toString());
    server = await this.serversRepository.save(server)

    return this.findById(server.id)
  }

  //Delete specific server
  //Return null exception if success; otherwise return HttpException
  async delete (id: number) : Promise<HttpException> {

    //Set hostname null before softremove.
    let result = await this.update(id,{hostname: null});
    if (result instanceof HttpException) {return result}
    
    let affected = (await this.serversRepository.softDelete(id)).affected;
    if (affected <= 0) return new NotFoundException("No server found with id ("+id+")");

    //Notify HyperKube, don't wait for awnser
    this.hyperkubeService.notifyHyperkube(id.toString());

    return null;
  }
}
