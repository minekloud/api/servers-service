import { Test, TestingModule } from '@nestjs/testing';
import { CommandController } from './command.controller';
import { HyperkubeService } from './services/hyperkube.service';

describe('CommandController', () => {
  let controller: CommandController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CommandController],
      providers: [HyperkubeService]
    })
    .overrideProvider(HyperkubeService)
    .useValue(jest.fn())
    .compile();

    controller = module.get<CommandController>(CommandController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
