import { Module,HttpModule } from '@nestjs/common';
import { ServerController } from './server.controller';
import { ServerService } from './services/server.service';
import {HyperkubeService} from './services/hyperkube.service'
import { Server } from './entities/server.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { CommandController } from './command.controller';
import { PlayerService } from './services/player.service';
import { PlayerController } from './player.controller';


@Module({
  imports: [TypeOrmModule.forFeature([Server]),ConfigModule,HttpModule],
  controllers: [ServerController, CommandController, PlayerController],
  providers: [ServerService,HyperkubeService,PlayerService],
})
export class ServerModule {}