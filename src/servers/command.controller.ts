import { Body, Controller, HttpCode, Param, ParseIntPipe, Post, ServiceUnavailableException } from '@nestjs/common';
import { CommandDto } from './dto/command.dto';
import { HyperkubeService } from './services/hyperkube.service';

@Controller()
export class CommandController {
  constructor(private hyperkubeService: HyperkubeService) {}

  @Post('servers/:id/sendcommand')
  @HttpCode(200)
  async command(
    @Param('id', ParseIntPipe) id: number,
    @Body() commandDto: CommandDto,
  ) {
    // todo verify user permission
    const result = await this.hyperkubeService.sendCommand(id.toString(), commandDto.command);
    if (!result) {
        throw new ServiceUnavailableException();
    }

    return;
  }
}
