import { NotFoundException,ConflictException, Query, HttpException } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import {UpdateServerDto} from './dto/update-server.dto'
import { Server } from './entities/server.entity'
import { ServerController } from './server.controller';
import { HyperkubeService } from './services/hyperkube.service';
import { ServerService } from './services/server.service';
import { ServerStatus } from './models/serverStatus';
import { PlayerService } from './services/player.service';
import { ServerDto } from './dto/server.dto'

describe('ServerController', () => {
  let serverController: ServerController;
  let serverService: ServerService;

  const mockRepository = jest.fn().mockReturnValue({
    find: jest.fn()
  })
  const mockHyperkubeService = jest.fn().mockReturnValue({});


  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ServerController],
      providers: [ServerService, {
        provide: getRepositoryToken(Server),
        useValue: mockRepository
      },HyperkubeService, PlayerService]
    })
    .overrideProvider(HyperkubeService)
    .useValue(mockHyperkubeService)
    .compile();

    serverService = module.get<ServerService>(ServerService)
    serverController = module.get<ServerController>(ServerController);
  });

  it('find shoud return what serverService return if found', async () => {
    let servers: ServerDto[] = []
    jest.spyOn(serverService, 'find').mockImplementation(async () => {return servers});
    expect(await serverController.find(Query())).toMatchObject(servers)
  });

  it('find should return if no server found by Serverservice', async () => {
    jest.spyOn(serverService, 'find').mockImplementation(async () => {return []});
    await expect(await serverController.find(Query())).toEqual([]);
  });

  it('findById shoud return server object if found', async () => {
    let server = new ServerDto()
    jest.spyOn(serverService, 'findById') .mockImplementation(async () => {return server});
    expect(await serverController.findById(1)).toMatchObject(server)
  });

  it('findById should throw notfoundexception if no server found by Serverservice', async () => {
    jest.spyOn(serverService, 'findById') .mockImplementation(async () => {return null});
    await expect(serverController.findById(1))
    .rejects
    .toThrow(NotFoundException);
  });

  it('create should return undefined if serverService return null', async () => {
    let createServerDto = {idUser:1,name:'sdee',hostname:'sss',maxAvailableRAM:1};
    jest.spyOn(serverService, 'create') .mockImplementation(async () => {return null});
    expect(await serverController.create(createServerDto)).toBeUndefined();
  });

  it('create should throw Exception if serverService return exception object', async () => {
    let createServerDto = {idUser:1,name:'test',hostname:'testHostname',maxAvailableRAM:1};
    jest.spyOn(serverService, 'create') .mockImplementation(async () => {return new ConflictException()});
    await expect(serverController.create(createServerDto))
    .rejects
    .toThrow(HttpException);
  });

  it('update should return what service update return ', async () => {
    let updateServerDto : UpdateServerDto;
    let serverDto: ServerDto = {name: 'test',hostname:'testHostname',status:ServerStatus.OFF,maxAvailableRAM:1,id:1,idUser:1,deletedAt:null, autoStart:false, autoStop: true, players: []}
    jest.spyOn(serverService, 'update') .mockImplementation(async () => {return serverDto});
    expect(await serverController.update(serverDto.id,updateServerDto)).toMatchObject(serverDto);
  });

  it('update should throw Exception if serverService return exception object', async () => {
    let updateServerDto : UpdateServerDto;
    jest.spyOn(serverService, 'update') .mockImplementation(async () => {return new NotFoundException()});
    await expect(serverController.update(1,updateServerDto))
    .rejects
    .toThrow(HttpException);
  });

  it('delete should return undefined if serverService does not return exception', async () => {
    jest.spyOn(serverService, 'delete') .mockImplementation(async () => {return null});
    expect(await serverController.delete(1)).toBeUndefined()
  });

  it('delete should throw Exception if serverService return exception object', async () => {
    jest.spyOn(serverService, 'delete') .mockImplementation(async () => {return new NotFoundException});
    await expect(serverController.delete(1))
    .rejects
    .toThrow(HttpException)
  });
});
