import { Module } from '@nestjs/common';
import { ServerModule} from './servers/server.module'
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from '../ormconfig';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [TypeOrmModule.forRoot({
    ...ormconfig,
    type: 'postgres',
    autoLoadEntities: true}),
    ServerModule,
    ConfigModule.forRoot({isGlobal: true,
    envFilePath:'config.env'})
  ],
})
export class AppModule {}
